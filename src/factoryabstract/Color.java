package factoryabstract;

public interface Color {
    void fill();
}
