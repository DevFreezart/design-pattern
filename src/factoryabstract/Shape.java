package factoryabstract;

public interface Shape {
    void draw();
}
