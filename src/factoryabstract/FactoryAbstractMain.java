package factoryabstract;

public class FactoryAbstractMain {

    public static void main(String[] args) {
        AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");

        Shape shapeCircle = shapeFactory.getShape("Circle");
        Shape shapeRectangle = shapeFactory.getShape("Rectangle");
        Shape shapeSquare = shapeFactory.getShape("Square");

        shapeCircle.draw();
        shapeRectangle.draw();
        shapeSquare.draw();

        AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

        Color colorRed = colorFactory.getColor("Red");
        Color colorBlue = colorFactory.getColor("Blue");
        Color colorOrange = colorFactory.getColor("Orange");

        colorBlue.fill();
        colorRed.fill();
        colorOrange.fill();
    }

}
