package factoryabstract;

public class Orange implements Color {
    @Override
    public void fill() {
        System.out.println("Orange");
    }
}
