package singleton;

public class SingletonMain {

    public static void main(String[] args) {
        SingletonObject object = SingletonObject.getInstance();

        object.showMessage();
    }

}
