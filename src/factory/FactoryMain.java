package factory;

public class FactoryMain {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();

        Shape shapeCircle = shapeFactory.getShape("Circle");
        Shape shapeRectangle = shapeFactory.getShape("Rectangle");
        Shape shapeSquare = shapeFactory.getShape("Square");

        shapeCircle.draw();
        shapeRectangle.draw();
        shapeSquare.draw();

    }
}
